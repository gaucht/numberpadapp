package numberpadserver;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ServerProtocol extends Thread{
	
	private Socket client = null;
	
	public ServerProtocol(Socket client)
	{
		super("ClientHandler");
		System.out.println("Processing client");
		this.client = client;
	}
	public void run()
	{
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			BufferedWriter out = new BufferedWriter( new OutputStreamWriter(this.client.getOutputStream()));
			   String fromUser;
			   String inputString = "";
		        while ((fromUser = in.readLine()) != null) {
		            	
		            	//System.out.println(fromUser);
		            	inputString += fromUser;
		            	if(fromUser.isEmpty())
		            	{
		            		break;
		            	}
		            }
		        int key = this.getKeyValue(inputString);
		        System.out.println(key);
		        this.processInput(key);
		        out.write("HTTP/1.1 200 OK\r\n");
		        out.write("Content-Type: text/plain\r\n");
		        out.write("Content-Length: 7\r\n");
		        out.write("Success\r\n");
		        this.client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        
        
     
	}
	public int getKeyValue(String inputString)
	{
		System.out.println(inputString);
		String postValue = inputString.substring(inputString.indexOf("[")+1, inputString.indexOf("]"));
		
		return Integer.parseInt(postValue);
	}
	public void processInput(int key)
	{	try {
        	Robot robot = new Robot();
        	
			switch(key)
			{
			case 1:
	        	robot.keyPress(KeyEvent.VK_1);
	        	robot.keyRelease(KeyEvent.VK_1);
				break;
			case 2:
	        	robot.keyPress(KeyEvent.VK_2);
	        	robot.keyRelease(KeyEvent.VK_2);
				break;
			case 3:
	        	robot.keyPress(KeyEvent.VK_3);
	        	robot.keyRelease(KeyEvent.VK_3);
				break;
			case 4:
	        	robot.keyPress(KeyEvent.VK_4);
	        	robot.keyRelease(KeyEvent.VK_4);
				break;
			case 5:
	        	robot.keyPress(KeyEvent.VK_5);
	        	robot.keyRelease(KeyEvent.VK_5);
				break;
			case 6:
	        	robot.keyPress(KeyEvent.VK_6);
	        	robot.keyRelease(KeyEvent.VK_6);
				break;
			case 7:
	        	robot.keyPress(KeyEvent.VK_7);
	        	robot.keyRelease(KeyEvent.VK_7);
				break;
			case 8:
	        	robot.keyPress(KeyEvent.VK_8);
	        	robot.keyRelease(KeyEvent.VK_8);
				break;
			case 9:
	        	robot.keyPress(KeyEvent.VK_9);
	        	robot.keyRelease(KeyEvent.VK_9);
				break;
			case 0:
	        	robot.keyPress(KeyEvent.VK_0);
	        	robot.keyRelease(KeyEvent.VK_0);
				break;
			case 10:
				robot.keyPress(KeyEvent.VK_ENTER);
	        	robot.keyRelease(KeyEvent.VK_ENTER);
			}
		
			} catch (AWTException e) {
		        e.printStackTrace();
		}
	}
}
