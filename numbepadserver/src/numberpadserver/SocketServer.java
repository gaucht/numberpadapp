package numberpadserver;

import java.net.*;
import java.io.*;

import javax.swing.JOptionPane;
 
public class SocketServer {
	private int portNumber;
	boolean connected;
	ServerSocket serverSocket;
    public SocketServer(int portNumber) throws IOException {
     	this.portNumber = portNumber;
     	connected = false;
    }
    public boolean start() throws IOException
    {
    		if(!connected)
    		{
    			serverSocket = new ServerSocket(portNumber);
    			System.out.println("Connected on port: " + portNumber);
    			connected = true;
    		}
    		while(true)
    		{
	            System.out.println("Waiting for client...");
	            Socket clientSocket = serverSocket.accept();
	            System.out.println("Client Connected");

	              
	            // Initiate conversation with client
	            ServerProtocol kkp = new ServerProtocol(clientSocket);
	            kkp.start();
    		}
           
    }
}